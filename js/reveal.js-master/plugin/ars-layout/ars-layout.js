"use strict";


const ArsLayout = {
  init: function() {
    let header = this.getHeaderTemplate();
    let footer = this.getFooterTemplate();
    this.getAllSlides().forEach(this.insertLayout(header, footer));
  },

  insertLayout: function oinkmen(header, footer) {
    return function(slideNode, pageNum) {
      let parent = slideNode.parentNode;
      let slideContentNodes = slideNode.childNodes;
      let newSlideContainer = slideNode.cloneNode();
      const insertHeader = this.setSlideHeader(slideNode, header.cloneNode(true));
      newSlideContainer.appendChild(insertHeader);

      const insertFooter = this.setSlideFooter(pageNum + 1, footer.cloneNode(true));

      let contentNode = document.createElement("main");
      contentNode.setAttribute('class', 'arslayout--slidecontent');
      for (let i=0; i < slideContentNodes.length; i++) {
        contentNode.appendChild(slideContentNodes[i].cloneNode(true));
      }
      newSlideContainer.appendChild(contentNode);
      newSlideContainer.appendChild(insertFooter);
      parent.replaceChild(newSlideContainer, slideNode)
    }.bind(this);
  },

  setSlideHeader: function(slideNode, headerTpl) {
    const slideTitleNode = slideNode.querySelector('h2.slide-title');
    if (slideTitleNode) {
      const slideTitle = slideTitleNode.innerText;
      const targetNode = headerTpl.querySelector('.arslayout--header_slidetitle');
      if (targetNode) targetNode.innerText = slideTitle;
    }
    return headerTpl;
  },

  setSlideFooter: function(pageNum, footerTpl) {
    const slideFooterNode = footerTpl.querySelector('.arslayout--footer_slidepagenumber');
    if (slideFooterNode) {
      slideFooterNode.innerText = pageNum;
    }
    return footerTpl;
  },

  getAllSlides: function() {
    let allSlides = [];
    let selection = document.querySelector('.slides').childNodes;
    for (var i=0; i < selection.length; i++) {
      if (selection[i].nodeType == Node.ELEMENT_NODE) {
        allSlides.push(selection[i]);
      }
    }
    return allSlides;
  },

  getHeaderTemplate: function() {
    return this.prepareLayoutTemplate('header');
  },

  getFooterTemplate: function() {
    return this.prepareLayoutTemplate('footer');
  },

  prepareLayoutTemplate: function(layoutpart) {
    let tpl = document.getElementById('layout-' + layoutpart).childNodes[1];
    return this.substituteTemplateVariables([tpl])[0];
  },

  substituteTemplateVariables: function(nodeList) {
    nodeList.forEach((node, idx, listObj) => {
      switch (node.nodeType) {
        case Node.ELEMENT_NODE:
          this.substituteTemplateVariables(node.childNodes);
          break;

        case Node.TEXT_NODE:
          for (var k in arsSettings) {
            node.nodeValue = node.nodeValue.replace('\{\{' + k + '\}\}', arsSettings[k]);
          }
          break;

        default:
          break;
      }
    });

    return nodeList;
  }


};
